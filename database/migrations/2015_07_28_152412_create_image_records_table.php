<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_records', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('type', [ 'jpg', 'png', 'gif' ]);
            $table->integer('width');
            $table->integer('height');
            $table->integer('filesize');
            $table->string('alt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('image_records');
    }
}
