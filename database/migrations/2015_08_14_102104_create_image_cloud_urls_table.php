<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageCloudUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_cloud_urls', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('image_record_id');
            $table->foreign('image_record_id')->references('id')->on('image_records');
            $table->integer('width');
            $table->integer('height');
            $table->string('cloud_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('image_cloud_urls');
    }
}
