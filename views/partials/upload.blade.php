<div class="image-upload image-upload-{{ $fileKey }}">
    <input type="hidden" name="image[{{ $fileKey }}][crop]" class="crop-data">

    <div class="image-preview">
        @if(isset($image) && $image)
            <img src="{{ $image->getUrl() }}" alt="{{ $image->alt }}">
        @endif
    </div>

    @if(isset($buttonView) && !empty($buttonView))
        @include($buttonView)
    @else
        <a class="ui green labeled icon upload-image button">
            <i class="upload icon"></i>
            Upload image
        </a>
    @endif

    <div class="ui modal">
        <div class="header">
            Upload image
        </div>
        <div class="content">
            <label class="upload-image-area">
                <input type="file" name="{{ $fileKey }}">
            </label>

            <div class="crop-container">

            </div>

            <div class="actions-container">
                <div class="ui hidden divider"></div>

                <a class="ui positive icon maximize action button" data-content="Select whole image">
                    <i class="maximize icon"></i>
                </a>
            </div>

            <div class="alt-text-container">
                <div class="ui divider"></div>

                <div class="ui form">
                    <div class="field">
                        <label>Image alt text</label>

                        <div class="ui input">
                            <input type="text" name="image[{{ $fileKey }}][alt]" placeholder="Image alt text">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="actions">
            <div class="ui black deny button">
                Cancel
            </div>
            <div class="ui positive right labeled icon button">
                Confirm
                <i class="checkmark icon"></i>
            </div>
        </div>
    </div>
</div>