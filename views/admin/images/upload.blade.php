@extends('admin::layouts.default')

@section('content.header')
    <h1 class="ui header">Upload image</h1>
@stop

@section('content.inner')
    <form method="post" enctype="multipart/form-data" action="">
        {!! csrf_field() !!}

        @include('image::partials.upload', [ 'fileKey' => 'pic' ])

        <button>Submit</button>
    </form>
@stop

@section('scripts')
    <style>

    </style>

    <script>
        $(document).ready(function() {
            var upload = new ImageUpload($('.image-upload'));
            //upload.setCropSize(200, 400);
        });
    </script>
@stop