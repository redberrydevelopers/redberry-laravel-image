# redberry/laravel-image

This package provides a way for users to easily upload and crop images within the website.

## Installation

Add the repository:

```shell
$ composer config repositories.image vcs git@bitbucket.org:redberrydevelopers/redberry-laravel-image.git
```

Require the package in composer:

```shell
$ composer require redberry/laravel-image:dev-master
```

Add the service provider to your config/app.php BEFORE your application service providers

```php
'providers' => [
    ...
    Redberry\Image\ImageServiceProvider::class,

    /*
     * Application Service Providers...
     */
    App\Providers\AppServiceProvider::class,
    ...
],
```

Publish the package assets

```shell
$ php artisan vendor:publish
```

Run the migrations
```shell
$ php artisan migrate --path=database/migrations/vendor/redberry/laravel-image
```

You should add an Apache alias to the storage/images folder
```
...
Alias /images /var/www/storage/images
...
```

Also make sure this folder is server-writable.

Make sure you include the following JavaScript and CSS files on pages where you want to use the laravel image block. If using laravel-admin add them to config/admin/assets.


```
#!php 
 // CSS
'packages/redberry/laravel-image/vendor/cropper/0.10.1/cropper.css',
'packages/redberry/laravel-image/css/image.css',

// JavaScript
'packages/redberry/laravel-image/vendor/cropper/0.10.1/cropper.js',
'packages/redberry/laravel-image/js/image.js',

```

Initialise the ImageUpload in your custom .js file after JQuery document ready
```
#!php
 $(document).ready(function() {
  var logoUpload = new ImageUpload($('.image-upload-logo'));
 });
```

## Usage

View -

In your edit or create view add an image field to your form. $logoImage is an instance of the image object, if you don't have an image object yet put null. 'logo' is the name which can be anything you like
```
#!php
<div class="field">
    <label>Logo</label>

    @include('image::partials.upload', [ 'fileKey' => 'logo', 'image' => $logoImage ])
</div>
```
Make sure you set the form enctype to allow file uploads
```
#!php
<form action="{{ $saveUrl }}" method="post" class="ui form" ng-controller="EventsController" enctype="multipart/form-data">
```

Model -

Add the corresponding field to your model database table e.g. 'logo_image_id', type = integer

Use the ImageRecord class in your model
```
#!php
// Redberry Blocks
use Redberry\Image\Models\ImageRecord;
```

Add a method on your model to retrieve the image using Laravel relationships
```
#!php
public function logo() {
    return $this->belongsTo(ImageRecord::class, 'logo_image_id');
}
```

Controller -

In order to save the image to the model, inject the ImageUpload class into your controller

```
#!php
use Redberry\Image\Upload\ImageUpload;

class EventsController extends Controller {

    /** @var  ImageUpload $imageUpload */
    protected $imageUpload;

    public function __construct(ImageUpload $imageUpload) {
        $this->imageUpload = $imageUpload;
    } 

   ...

```

On your save method handle the image input

```
#!php
// Handle logo
if($request->hasFile('logo')) {
    $image = $this->imageUpload->upload($request, 'logo');
    $event->logo_image_id = $image->id;
}

...

$event->save();
```

To retrieve the image object

```
#!php
$event->logo
```