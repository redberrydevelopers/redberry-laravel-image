function ImageUpload(container) {
    (function(scope) {

        scope.container = container; // .image-upload element
        scope.modal = null; // Modal box element (.ui.modal)
        scope.fileInput = null; // input[type="file"] element
        scope.imagePreview = null; // Image preview container
        scope.cropDataInput = null; // <input/> for crop state
        scope.cropImage = null; // <img/> for crop tool
        scope.cropAspectRatio = null;
        scope.previewMaxWidth = null;
        scope.previewMaxHeight = null;

        /*
         * Pseudo-constructor
         */
        scope.ctor = function() {
            // Modal
            scope.modal = scope.container.find('.ui.modal');
            scope.modal.modal({
                detachable: false, // Make sure the modal stays inside the <form/>
                onApprove: scope.events.onModalApprove,
                onDeny: scope.events.onModalDeny,
                observeChanges: true
            });

            // "Upload image" button
            scope.container.find('.upload-image').click(scope.events.onUploadImageClick);

            // File input
            scope.fileInput = scope.modal.find('.upload-image-area input[type="file"]');
            scope.fileInput.change(scope.events.onFileInputChange);

            // Crop data input
            scope.cropDataInput = scope.container.find('input.crop-data');

            // Image preview
            scope.imagePreview = scope.container.find('.image-preview');
            scope.updatePreview();

            // Action buttons
            scope.container.find('.maximize.action.button').click(scope.events.onMaximizeClick);

            // Popup on action buttons
            scope.container.find('.action.button[data-content]').popup();
        };

        scope.setCropAspectRatio = function(aspectRatio) {
            scope.cropAspectRatio = aspectRatio;
        };

        scope.setCropSize = function(width, height) {
            scope.cropAspectRatio = width / height;
        };

        scope.hideAltText = function() {
            scope.modal.addClass('alt-text-hidden');
        };

        scope.loadImageUrl = function(url) {
            // Hide upload area
            scope.modal.find('.upload-image-area').hide();

            scope.loadCropTool(url);
        };

        scope.loadCropTool = function(imageUrl) {
            var cropContainer = scope.modal.find('.crop-container');

            // Create new <img/>
            scope.cropImage = $('<img class="crop-image" />');
            scope.cropImage.prop('src', imageUrl);
            cropContainer.append(scope.cropImage);

            var cropOptions = { };

            cropOptions.zoomable = false;

            cropOptions.aspectRatio = scope.cropAspectRatio;

            cropOptions.crop = scope.events.onCropperChange;

            cropOptions.built = scope.events.onCropperBuilt;

            // Init cropper
            scope.cropImage.cropper(cropOptions);
        };

        scope.resetModal = function() {
            if(scope.cropImage) {
                scope.cropImage.cropper('destroy');
                scope.cropImage.remove();
                scope.cropImage = null;
            }

            // Show upload area
            scope.modal.find('.upload-image-area').show();
        };

        scope.maximizeCropBox = function() {
            if(scope.cropImage && scope.cropImage) {
                var canvasData = scope.cropImage.cropper('getCanvasData');

                // Set width/height to the size of the canvas
                var cropBoxData = {
                    width: canvasData.width,
                    height: canvasData.height
                };

                // Setting the data here will take the input and change them according to the aspect ratio setting (if set)
                // This will always make the crop box as big as possible
                scope.cropImage.cropper('setCropBoxData', cropBoxData);

                // Get the data back to get the new calculated values
                cropBoxData = scope.cropImage.cropper('getCropBoxData');

                // Make the position to the center of the image
                cropBoxData.left = (canvasData.left + (canvasData.width / 2)) - (cropBoxData.width / 2);
                cropBoxData.top = (canvasData.top + (canvasData.height / 2)) - (cropBoxData.height / 2);

                // Set it again
                scope.cropImage.cropper('setCropBoxData', cropBoxData);
            }
        };

        /**
         * Apply configuration options to preview image
         */
        scope.updatePreview = function() {
            $(scope.imagePreview).each(function() {
                var preview = $(this);

                preview.find('img').each(function() {
                    var previewImage = $(this);

                    previewImage.css({
                        maxWidth: scope.previewMaxWidth,
                        maxHeight: scope.previewMaxHeight
                    });
                });
            });
        };

        scope.setPreviewMaxWidth = function(maxWidth) {
            scope.previewMaxWidth = maxWidth;
            scope.updatePreview();
        };

        scope.setPreviewMaxHeight = function(maxHeight) {
            scope.previewMaxHeight = maxHeight;
            scope.updatePreview();
        };

        /*
         * Event handlers
         */
        scope.events = {

            onUploadImageClick: function() {
                // Clear file selection
                scope.fileInput.val('');

                // Show modal window
                scope.modal.modal('show');
            },

            onFileInputChange: function(event) {
                if(event.target.files && event.target.files.length > 0) {
                    // Read the file as a data URL
                    var reader = new FileReader();
                    reader.onload = scope.events.onFileLoaded;
                    reader.readAsDataURL(event.target.files[0]);
                }
            },

            onFileLoaded: function(event) {
                var dataUrl = this.result;
                scope.loadImageUrl(dataUrl);
            },

            onCropperChange: function(event) {
                var data = scope.cropImage.cropper('getData', true);
                scope.cropDataInput.val(JSON.stringify(data));
            },

            onCropperBuilt: function(event) {
                scope.maximizeCropBox();
            },

            onModalApprove: function(event) {
                if(scope.cropImage) {
                    var preview = scope.cropImage.cropper('getCroppedCanvas');

                    // Clear previous
                    scope.imagePreview.empty();

                    // Create new <img/> preview
                    var img = $('<img/>');
                    img.prop('src', preview.toDataURL());

                    // Add preview to DOM
                    scope.imagePreview.append(img);

                    scope.updatePreview();
                }

                scope.resetModal();
            },

            onModalDeny: function(event) {
                scope.resetModal();
            },

            onMaximizeClick: function() {
                scope.maximizeCropBox();

                return false;
            }

        };

        scope.ctor();

    })(this);
}