<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 28/07/2015
 * Time: 14:26
 */

Route::model('image', \Redberry\Image\Models\ImageRecord::class);

Route::group([ 'as' => 'image::', 'namespace' => 'Redberry\Image\Controllers' ], function() {

    Route::get('image/resize/{image}', [ 'as' => 'image.resize', 'uses' => 'ImagesController@resize' ]);

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {

        /*
         * Requires authenticated user
         */
        Route::group(['middleware' => Redberry\Admin\Middleware\CanAccessAdmin::class], function () {

            Route::get('images/upload', ['as' => 'admin.images.upload', 'uses' => 'ImagesController@upload']);

            Route::post('images/upload', ['as' => 'admin.images.postUpload', 'uses' => 'ImagesController@postUpload']);

        });

    });

});