<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 13/08/2015
 * Time: 17:47
 */

namespace Redberry\Image\Cloud;


use Guzzle\Http\Exception\ClientErrorResponseException;
use OpenCloud\ObjectStore\Constants\UrlType;
use OpenCloud\ObjectStore\Resource\DataObject;
use Redberry\Image\Models\ImageCloudUrl;
use Redberry\Image\Models\ImageRecord;
use Redberry\Image\Upload\ImageStore;

class CloudUpload {

    public static function uploadImageRecord(ImageRecord $imageRecord) {
        // Name of object in the container
        $objectName = ImageStore::imageRecordObjectName($imageRecord);

        // File to upload
        $filePath = $imageRecord->getFilePath();

        return self::uploadFileToCloud($objectName, $filePath);
    }

    public static function uploadResizedImageRecord(ImageRecord $imageRecord, $width, $height) {
        // Name of object in the container
        $objectName = ImageStore::imageRecordResizedObjectName($imageRecord, $width, $height);

        // File to upload
        $filePath = ImageStore::imageRecordResizedFilePath($imageRecord, $width, $height);

        $url = self::uploadFileToCloud($objectName, $filePath);

        $record = ImageCloudUrl::create([
            'image_record_id' => $imageRecord->id,
            'width' => $width,
            'height' => $height,
            'cloud_url' => $url,
        ]);

        return $record;
    }

    protected static function uploadFileToCloud($objectName, $filePath) {
        $container = self::getCreateContainer();

        // Check if there's an object with the same name already on there
        if($container->objectExists($objectName)) {
            /** @var DataObject $object */
            $object = $container->getPartialObject($objectName);

            // If the object exists, but the hash doesn't match then throw an exception
            if($object->getEtag() !== md5_file($filePath)) {
                throw new \Exception('File conflict on object storage: '.$container->name.'/'.$objectName);
            }
        }
        else {
            // Upload new object
            $object = $container->uploadObject($objectName, fopen($filePath, 'r+'));
        }

        return (string)$object->getPublicUrl(UrlType::SSL);
    }

    protected static function getCreateContainer() {
        $objectStore = OpenStackServiceFactory::getObjectStorageService();

        $containerName = config('images.cloud.container');

        try {
            // Try get the container
            $container = $objectStore->getContainer($containerName);
        }
        catch(ClientErrorResponseException $exception) {
            // HTTP error

            if($exception->getResponse()->getStatusCode() === 404) {
                // It's a 404 - container doesn't exist so create it with CDN enabled
                $container = $objectStore->createContainer($containerName, [ 'X-CDN-Enabled' => 'True' ]);
            }
            else {
                // Another HTTP error - throw the exception
                throw $exception;
            }
        }

        return $container;
    }

}