<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 14/08/2015
 * Time: 10:49
 */

namespace Redberry\Image\Cloud;


use OpenCloud\Rackspace;

class OpenStackServiceFactory {

    public static function getObjectStorageService() {
        $clientName = config('images.cloud.client');

        if($clientName === 'rackspace') {
            $client = new Rackspace(Rackspace::UK_IDENTITY_ENDPOINT, [
                'username' => config('images.cloud.clients.rackspace.username'),
                'apiKey' => config('images.cloud.clients.rackspace.apiKey'),
            ]);

            $objectStore = $client->objectStoreService(null, 'LON');
        }
        else {
            throw new \Exception('Invalid client: '.$clientName);
        }

        return $objectStore;
    }

}