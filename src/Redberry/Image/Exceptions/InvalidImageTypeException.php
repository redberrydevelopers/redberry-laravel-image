<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 28/07/2015
 * Time: 17:21
 */

namespace Redberry\Image\Exceptions;


class InvalidImageTypeException extends \Exception {

    public function __construct($type) {
        $message = 'Invalid image type: '.$type;

        parent::__construct($message);
    }

}