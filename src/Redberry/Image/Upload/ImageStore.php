<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 28/07/2015
 * Time: 16:30
 */

namespace Redberry\Image\Upload;


use Carbon\Carbon;
use Redberry\Image\Models\ImageRecord;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class ImageStore {

    public static function path() {
        return rtrim(config('images.store.path'), '\\/');
    }

    public static function url() {
        return rtrim(config('images.store.url'), '\\/');
    }

    public static function datePath(Carbon $date) {
        return self::path().'/'.self::staggerDate($date);
    }

    public static function dateResizedPath(Carbon $date) {
        return self::path().'/resized/'.self::staggerDate($date);
    }

    public static function dateFilePath(Carbon $date, $name) {
        return self::datePath($date).'/'.$name;
    }

    public static function dateResizedFilePath(Carbon $date, $name) {
        return self::dateResizedPath($date).'/'.$name;
    }

    public static function uniqueDateFilePath(Carbon $date, $name) {
        $file = self::dateFilePath($date, $name);

        if(file_exists($file)) {
            $fileName = pathinfo($name, PATHINFO_FILENAME);
            $ext = pathinfo($name, PATHINFO_EXTENSION);

            // Append the time to the file
            $newName = $fileName.'-'.$date->format('H-i-s').'.'.$ext;

            // Send through this function again in case the new filename also exists!
            return self::uniqueDateFilePath($date, $newName);
        }
        else {
            return $name;
        }
    }

    public static function imageRecordPath(ImageRecord $record) {
        return self::datePath($record->created_at);
    }

    public static function imageRecordResizedPath(ImageRecord $record) {
        return self::dateResizedPath($record->created_at);
    }

    public static function createImageRecordPath(ImageRecord $record) {
        return self::createPath(self::imageRecordPath($record));
    }

    public static function createImageRecordResizedPath(ImageRecord $record) {
        return self::createPath(self::imageRecordResizedPath($record));
    }

    public static function imageRecordFilePath(ImageRecord $record) {
        return self::dateFilePath($record->created_at, $record->name);
    }

    public static function imageRecordResizedFilePath(ImageRecord $record, $width, $height) {
        return self::dateResizedFilePath($record->created_at, self::resizedFileName($record->name, $width, $height));
    }

    public static function createPath($path) {
        if(!file_exists($path)) {
            if(!mkdir($path, 0777, true)) {
                throw new FileException(sprintf('Unable to create the "%s" directory', $path));
            }
        }

        return $path;
    }

    public static function resizedFileName($name, $width, $height) {
        $fileName = pathinfo($name, PATHINFO_FILENAME);
        $ext = pathinfo($name, PATHINFO_EXTENSION);

        return sprintf('%s-%dx%d.%s', $fileName, $width, $height, $ext);
    }

    public static function dateUrl(Carbon $date) {
        return self::url().'/'.self::staggerDate($date);
    }

    public static function dateResizedUrl(Carbon $date) {
        return self::url().'/resized/'.self::staggerDate($date);
    }

    public static function dateFileUrl(Carbon $date, $name) {
        return self::dateUrl($date).'/'.$name;
    }

    public static function dateResizedFileUrl(Carbon $date, $name) {
        return self::dateResizedUrl($date).'/'.$name;
    }

    public static function imageRecordUrl(ImageRecord $record) {
        return asset(self::dateFileUrl($record->created_at, $record->name));
    }

    public static function imageRecordResizedUrl(ImageRecord $record, $width, $height) {
        return asset(self::dateResizedFileUrl($record->created_at, self::resizedFileName($record->name, $width, $height)));
    }

    public static function imageRecordObjectName(ImageRecord $record) {
        return self::dateFileUrl($record->created_at, $record->name);
    }

    public static function imageRecordResizedObjectName(ImageRecord $record, $width, $height) {
        return self::dateResizedFileUrl($record->created_at, self::resizedFileName($record->name, $width, $height));
    }

    public static function staggerDate(Carbon $date) {
        return $date->format('Y/m/d');
    }

}