<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 28/07/2015
 * Time: 16:30
 */

namespace Redberry\Image\Upload;

use Redberry\Image\Cloud\CloudUpload;
use Redberry\Image\Models\ImageRecord;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Http\Request;
use Image;
use \Intervention\Image\Image as InterventionImage;

class ImageUpload {

    public function upload(Request $request, $key) {
        if(!$request->hasFile($key)) {
            throw new \Exception('No file provided: '.$key);
        }

        $file = $request->file($key);
        $image = Image::make($file);

        $altText = null;

        // See if there's any image params
        if($request->has('image')) {
            $images = $request->get('image');

            if(is_array($images) && array_key_exists($key, $images)) {
                $imageParams = $images[$key]; // Get params for this specific image

                if(is_array($imageParams)) {
                    // Alt text
                    if(array_key_exists('alt', $imageParams)) {
                        if(!empty($imageParams['alt'])) {
                            $altText = $imageParams['alt'];
                        }
                    }

                    // Crop
                    if(array_key_exists('crop', $imageParams)) {
                        $cropParams = json_decode($imageParams['crop']);

                        if($cropParams->rotate !== 0) {
                            // TODO
                            throw new \Exception('Rotation not implemented');
                        }

                        $shouldCrop = !($cropParams->x === 0 && $cropParams->y === 0 && $cropParams->width === $image->width() && $cropParams->height === $image->height());

                        if($shouldCrop) {
                            // Apply crop
                            $image->crop($cropParams->width, $cropParams->height, $cropParams->x, $cropParams->y);
                            $image->save();
                        }
                    }
                }
            }
        }

        $record = $this->uploadFile($file, $altText);

        return $record;
    }

    public function uploadFile(UploadedFile $file, $altText = null) {
        // Image record in DB
        $record = new ImageRecord();

        // Set date so we can use it to build the store path
        $record->created_at = $record->freshTimestamp();

        $fileName = $file->getClientOriginalName();

        // contains
        if (preg_match('/^[a-zA-Z0-9_.\- ]*$/', $fileName) == false) {
            $fileName = uniqid() . '.' . $file->getClientOriginalExtension();
        }

        // Get unique file name
        $record->name = ImageStore::uniqueDateFilePath($record->created_at, $fileName);

        // Load image details
        /** @var \Intervention\Image\Image $image */
        $image = Image::make($file->getPathname());

        // Image type
        $record->setTypeViaMime($image->mime());

        // Dimens
        $record->width = $image->width();
        $record->height = $image->height();

        // File info
        $record->filesize = $file->getSize();

        // Alt text
        $record->alt = $altText;

        // Destination file path
        $dir = ImageStore::createImageRecordPath($record);

        // Move into place
        $file->move($dir, $record->name);

        // Upload to cloud if enabled
        if(config('images.cloud.enabled')) {
            $url = CloudUpload::uploadImageRecord($record);
            $record->cloud_url = $url;
        }

        // Write record to DB
        $record->save();

        return $record;
    }

}