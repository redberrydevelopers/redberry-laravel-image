<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 22/07/2015
 * Time: 14:23
 */

namespace Redberry\Image;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class ImageServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot() {
        /*
         * Load routes
         */
        if (!$this->app->routesAreCached()) {
            require __DIR__.'/../../../routes.php';
        }

        /*
         * Load views
         */
        $this->app['view']->addNamespace('image', __DIR__.'/../../../views');

        /*
         * Publish config
         */
        $this->publishes([
            __DIR__.'/../../../config' => config_path('images'),
        ]);

        /*
         * Publish assets
         */
        $this->publishes([
            __DIR__.'/../../../assets' => public_path('packages/redberry/laravel-image'),
        ], 'public');

        /*
         * Copy migrations
         */
        $this->publishes([
            __DIR__.'/../../../database/migrations/' => database_path('migrations/vendor/redberry/laravel-image')
        ], 'migrations');

        /*
         * Load intervention/image
         */
        $this->app->register(\Intervention\Image\ImageServiceProvider::class); // Service provider

        // Facade
        AliasLoader::getInstance([
            'Image' => \Intervention\Image\Facades\Image::class
        ])->register();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {

    }

}