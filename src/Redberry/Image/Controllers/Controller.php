<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 12/08/2015
 * Time: 09:40
 */

namespace Redberry\Image\Controllers;


use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;
}