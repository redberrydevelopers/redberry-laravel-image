<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 12/08/2015
 * Time: 09:40
 */

namespace Redberry\Image\Controllers;


use Illuminate\Http\Request;
use Redberry\Image\Cloud\CloudUpload;
use Redberry\Image\Models\ImageRecord;
use Redberry\Image\Upload\ImageStore;

class ImagesController extends Controller {

    public function resize(Request $request, ImageRecord $imageRecord) {
        if(!$request->has('width')) {
            throw new \Exception('Missing width parameter');
        }
        elseif(!$request->has('height')) {
            throw new \Exception('Missing width parameter');
        }

        $width = $request->get('width');
        $height = $request->get('height');

        if(!is_numeric($width) || !is_numeric($height)) {
            throw new \Exception('Invalid value for width or height');
        }

        // Expected path of resized image
        $resizedPath = ImageStore::imageRecordResizedFilePath($imageRecord, $width, $height);

        if(file_exists($resizedPath)) {
            // If it already exists just return direct URL
            $url = ImageStore::imageRecordResizedUrl($imageRecord, $width, $height);
            return redirect($url);
        }
        else {
            // If not, generate resized version

            // Get image instance
            $image = $imageRecord->getImage();

            // Resize
            $image->resize($width, $height);

            // Create directories
            ImageStore::createImageRecordResizedPath($imageRecord);

            // Check this image hasn't been resized and saved while we were resizing ours
            if(!file_exists($resizedPath)) {
                $image->save($resizedPath);

                CloudUpload::uploadResizedImageRecord($imageRecord, $width, $height);
            }

            // Spit out the image directly
            return $image->response();
        }
    }

}