<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 28/07/2015
 * Time: 14:36
 */

namespace Redberry\Image\Controllers\Admin;

use Illuminate\Http\Request;
use Redberry\Admin\Controllers\BaseController;
use Redberry\Image\Upload\ImageUpload;

class ImagesController extends BaseController {

    /** @var  ImageUpload $imageUpload */
    protected $imageUpload;

    public function __construct(ImageUpload $imageUpload) {
        $this->imageUpload = $imageUpload;
    }

    public function upload() {
        return view('image::admin.images.upload');
    }

    public function postUpload(Request $request) {
        $result = null;

        if($request->hasFile('pic')) {
            $result = $this->imageUpload->upload($request, 'pic');
        }
    }

}