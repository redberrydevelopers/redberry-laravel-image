<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 28/07/2015
 * Time: 16:29
 */

namespace Redberry\Image\Models;


use Illuminate\Database\Eloquent\Model;
use Redberry\Image\Exceptions\InvalidImageTypeException;
use Redberry\Image\Upload\ImageStore;
use Image;

class ImageRecord extends Model {

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'created_at', 'updated_at' ];

    protected static $mimeToType = [
        'image/jpeg' => 'jpg',
        'image/png' => 'png',
        'image/gif' => 'gif',
    ];

    public function cloudUrls() {
        return $this->hasMany(ImageCloudUrl::class);
    }

    /**
     * Get \Intervention\Image\Image instance for this image record
     *
     * @return \Intervention\Image\Image
     */
    public function getImage() {
        return Image::make($this->getFilePath());
    }

    public function getUrl() {
        if($this->cloud_url && config('images.cloud.enabled')) {
            return $this->cloud_url;
        }
        else {
            return ImageStore::imageRecordUrl($this);
        }
    }

    public function getFilePath() {
        return ImageStore::imageRecordFilePath($this);
    }

    /**
     * Return a URL that will provide this image resized to the specified dimensions
     *
     * @param $width
     * @param $height
     * @return string
     */
    public function getResizedUrl($width, $height) {
        if(config('images.cloud.enabled')) {
            if($this->relationLoaded('cloudUrls')) {
                /*
                 * This code runs if you have already loaded the cloudUrls relationship, either by explicitly
                 * eager-loading it using with('cloudUrls'), or simply accessed the cloudUrls psuedo-property
                 * on this object.
                 *
                 * It ends up saving an extra query, which really means a lot especially when you have lots of
                 * images on a page. My recommendation for this situation is to always eager load using with(...)
                 * when displaying objects with images.
                 */
                $cloudUrlRecord = $this->cloudUrls->filter(function ($record) use ($width, $height) {
                    return ($record->width == $width && $record->height == $height);
                })->first();
            }
            else {
                // Otherwise query the db for the exact record
                $cloudUrlRecord = $this->cloudUrls()->width($width)->height($height)->first();
            }

            if($cloudUrlRecord) {
                return $cloudUrlRecord->cloud_url;
            }
        }

        // Expected path of resized image
        $resizedPath = ImageStore::imageRecordResizedFilePath($this, $width, $height);

        if(!file_exists($resizedPath)) {
            // If the resized version doesn't exist then provide a URL which will do the resizing
            return route('image::image.resize', [ 'image' => $this->id, 'width' => $width, 'height' => $height ]);
        }

        return ImageStore::imageRecordResizedUrl($this, $width, $height);
    }

    /**
     * Return a URL that will provide an image with dimensions being a minimum of those provided to this function
     *
     * @param $width
     * @param $height
     * @return string
     */
    public function getMinResizedUrl($width, $height) {
        $targetWidth = $width;
        $targetHeight = ($this->height / $this->width) * $targetWidth;

        if($targetHeight < $height) {
            $targetHeight = $height;
            $targetWidth = ($this->width / $this->height) * $targetHeight;
        }

        $targetWidth = round($targetWidth);
        $targetHeight = round($targetHeight);

        return $this->getResizedUrl($targetWidth, $targetHeight);
    }

    /**
     * Return a URL that will provide an image with dimensions being a maximum of those provided to this function
     *
     * @param $width
     * @param $height
     * @return string
     */
    public function getMaxResizedUrl($width, $height) {
        $targetWidth = $width;
        $targetHeight = ($this->height / $this->width) * $targetWidth;

        if($targetHeight > $height) {
            $targetHeight = $height;
            $targetWidth = ($this->width / $this->height) * $targetHeight;
        }

        $targetWidth = round($targetWidth);
        $targetHeight = round($targetHeight);

        return $this->getResizedUrl($targetWidth, $targetHeight);
    }

    public function setTypeViaMime($mime) {
        if(array_key_exists($mime, self::$mimeToType)) {
            $this->type = self::$mimeToType[$mime];
        }
        else {
            throw new InvalidImageTypeException($mime);
        }
    }

}