<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 14/08/2015
 * Time: 11:25
 */

namespace Redberry\Image\Models;

use Illuminate\Database\Eloquent\Model;

class ImageCloudUrl extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'image_record_id', 'width', 'height', 'cloud_url' ];

    public function scopeWidth($query, $width) {
        $query->where('width', '=', $width);

        return $query;
    }

    public function scopeHeight($query, $height) {
        $query->where('height', '=', $height);

        return $query;
    }

}