<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 28/07/2015
 * Time: 14:30
 */

return [

    'path' => env('IMAGE_STORE_PATH', storage_path('images')),

    'url' => env('IMAGE_STORE_URL', 'images'),

];