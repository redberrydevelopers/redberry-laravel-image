<?php
/**
 * Created by PhpStorm.
 * User: Oscar
 * Date: 13/08/2015
 * Time: 17:48
 */

return [

    'enabled' => env('IMAGE_CLOUD_ENABLED', false),

    'client' => 'rackspace',

    'clients' => [

        'rackspace' => [
            'username' => '{username}',
            'apiKey' => '{apiKey}',
        ],

    ],

    'container' => env('IMAGE_CLOUD_CONTAINER'),

];